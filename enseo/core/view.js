// Global page stack
window.pages = window.pages || new Array();

window.pages.topPage = function() {
    return this[this.length - 1];
};

window.pages.findPage = function(viewId) {
    var page = null;
    for(var i=0; i<window.pages.length; i++) {
        if(window.pages[i].viewId == viewId) {
            page = window.pages[i];
            break;
        }
    }
    return page;
};

window.pages.closePage = function(viewId) {
    var page = window.pages.findPage(viewId);
    if(page && page.destroy)
        page.destroy.call(page);
};

// Base view class
var View = Class.extend({
        
    // public properties
    id: 'view',  //eg: secondary
    
    viewId: null,  // In case we need multiple instances of same view. By default viewId = id
    
    wrapper: null, //eg: secondary-wrapper
    
    className: null,//eg: aboutus
    
    template: null, //eg: secondary.html
    
    css: null,      //eg: secondary.css
    
    data: null,     //eg: JSON object 
    
    breadcrumb: '',     //eg: Care|My Program
    
    label: '',          //eg: My Program
    
    query: '',          //eg: tag=hospitalmap&mapid=1
    
    oncreate: null,     // callback when page is shown
    
    ondestroy: null,    // callback when page is destroyed
    
    parent: null,   // parent page
    
    $el: null,  // cached jquery object
    
    itemSelector: 'a, .focusable',
    
    trackPageView: true,    // wether we should track pageview
    
    trackOptions: {},   // additional tracking options
    
    pagePath: null, //eg: /home/aboutus/leadership; if not provided we will guess by parent path + current className or viewId
    
    // overwrite functions
    
    /**
     * Overwrite to put constructor code. 
     */
    init: function (options) {
    },

    /**
     * Overwrite to put destructor code. 
     */
    uninit: function () {
    },
    
    /**
     * Overwrite to include render content logic. 
     */
    renderData: function () {
    },
    
    /**
     * Overwrite to process keys 
     */
    navigate: function (key) {
        return false;
    },
    
    /**
     * Overwrite to process link blur 
     */
    blur: function($jqobj) {
        this.$(this.itemSelector).filter('.active').removeClass('active');
        $(':focus').blur();
    },
    
    /**
     * Overwrite to process link focus
     */
    focus: function($jqobj) {
        if($jqobj && $jqobj.length > 0) {
            $jqobj.addClass('active');
            $jqobj.focus();
        }
    },
    
    /**
     * Overwrite to process clicks 
     */
    click: function($obj) {
        return false;
    },
    
    /**
     * Overwrite to do something after page is shown 
     */
    shown: function() {
    },
    
    // public functions
    
    $: function(selector) {
      return this.$el.find(selector);
    },
    
    render: function(data, success) {
        var context = this;
        if(this.css && $('link[href="css/' + this.css + '"]').length<1) {
            $("<link/>", {rel: "stylesheet", type: "text/css", href: "css/"+this.css}).appendTo("head");
        }
        else {
            this.css = null;
        }
        
        // $wrapper is not in DOM yet.  We will append to DOM when everything is done    
        var $wrapper = $('<div id="' + this.wrapper + '"></div>');
        
        if(this.template) { // load template
            $wrapper.load('templates/' + this.template + ' #' + this.id, function(responseText, textStatus, XMLHttpRequest) {
                context._ready($wrapper);
                context._track();
            });
        }
        else {
            $wrapper.append('<div id="' + this.id + '"></div>');
            this._ready($wrapper);
            this._track();
        }
        
    },
    
    refresh: function() {
    },
    
    initialize: function (options) {
        $.extend(this, options);
        
        this.viewId = this.viewId || this.id;
        this.wrapper = this.wrapper || (this.viewId + '-wrapper');
        
        this.init();
        
        if(!this.parent) {
            this.parent = window.pages.topPage();
        }
        
        if(!this.pagePath) {
            var path = this.className || this.viewId;
            var parentPath = this.parent? this.parent.pagePath: '';
            this.pagePath = parentPath + '/' + path;
        }
    },
    
    destroy: function(noParentTracking) {
        this.uninit();
        
        // restore key handler
        this._removeKeyListener();
        this._removeMouseListener();
        
        if(this.css) {
            $('link[href="css/' + this.css + '"]').remove();
        }
        
        $('#' + this.wrapper).remove();

        for(var i=0; i<window.pages.length; i++) {
            if(window.pages[i].viewId == this.viewId) {
                delete window.pages[i];
                window.pages.splice(i,1);
                break;
            }
        }
        
        if(!noParentTracking && this.parent) {
            this.parent._track();
        }
        
        this.parent = null;
        
         // callback to outside so they can bring up other UI
        if(this.ondestroy)
            this.ondestroy();
    },
    
    // handy helper functions
    
    // get the position of an item (usually an '<a>') inside a list
    getIndex: function(container, selector, focusSelector) {
        if(!container)
            container = '*';
        
        selector = selector || this.itemSelector;
        focusSelector = focusSelector || '.active';
        
        var $container = (container instanceof jQuery)? container : this.$(container);
        var $items = $container.find(selector);
        var $curr = $items.filter(focusSelector);
        var pos = $items.index($curr);
        return pos;
    },
    
    // move focus to previous or next item in the list
    changeFocus: function(key, container, selector, focusSelector) {
        if(!container)
            container = '*';
        
        selector = selector || this.itemSelector;
        focusSelector = focusSelector || '.active';
            
        var $container = (container instanceof jQuery)? container : this.$(container);                
        var $items = $container.find(selector).filter(':visible');
        var $curr = $items.filter(focusSelector);
        var pos = $items.index($curr);
        var $next = $curr;
        
        if(pos < 0) {
            $next = $($items.get(0));
            this.focus($next);
            return;
        }

        switch(key) {
            case 'UP':
            case 'LEFT':
                $next = $($items.get(pos-1));
                if($next.length <= 0)
                    $next = $($items.get(-1));
                break;
            case 'DOWN':
            case 'RIGHT':
                $next = $($items.get(pos+1));
                if($next.length <= 0)
                    $next = $($items.get(0));
                break;
        }
        this.blur($curr);
        this.focus($next);
    },
    
    focusNextPage: function(container, pageSize, totalSize, orientation, selector) {
        if(!container)
            return;
        if(!orientation)
            orientation = 'vertical';
        selector = selector || this.itemSelector;
            
        var $container = (container instanceof jQuery)? container : this.$(container);
        var idx = this.getIndex($container, selector);
        var total = totalSize;
        var page = Math.floor(idx / pageSize);
        
        page += 1;
        idx = page * pageSize;
        if(idx >= total)
            idx = total - 1;
        
        var $obj = $($container.find(selector).get(idx));
        this.blur($obj);
        this.focus($obj);
        this.scrollTo($obj, '#subselections', 'horizontal', true);
        return $obj;
    },
    
    focusPrevPage: function(container, pageSize, totalSize, orientation, selector) {
        if(!container)
            return;
        if(!orientation)
            orientation = 'vertical';
        selector = selector || this.itemSelector;
            
        var $container = (container instanceof jQuery)? container : this.$(container);
        var idx = this.getIndex($container, selector);
        var total = totalSize;
        var page = Math.floor(idx / pageSize);
        
        page -= 1;
        idx = page * pageSize;
        if(idx < 0)
            idx = 0;
        
        var $obj = $($container.find(selector).get(idx));
        this.blur($obj);
        this.focus($obj);
        this.scrollTo($obj, '#subselections', 'horizontal', true);
        return $obj;
    },
    
    // move scrollbar to next screen
    scrollNext: function(container, orientation) {
        if(!container)
            return;
        if(!orientation)
            orientation = 'vertical';
            
        var $container = (container instanceof jQuery)? container : this.$(container);
        if(orientation == 'horizontal') {
            $container.scrollLeft($container.scrollLeft() + $container.width());
        }
        else {
            $container.scrollTop($container.scrollTop() + $container.height());
        }
    },
    
    // move scrollbar to previous screen
    scrollPrev: function(container, orientation, autoFocus) {
        if(!container)
            return;
        if(!orientation)
            orientation = 'vertical';
            
        var $container = (container instanceof jQuery)? container : this.$(container);
        if(orientation == 'horizontal') {
            $container.scrollLeft($container.scrollLeft() - $container.width());
        }
        else {
            $container.scrollTop($container.scrollTop() - $container.height());
        }
    },
    
    // move scrollbar to the exact position of an item
    scrollTo: function($obj, container, orientation, autoFocus) {
        if(!$obj)
            return;
            
        if(!container)
            container = $obj.parent();
        if(!orientation)
            orientation = 'vertical';
        
        var $container = (container instanceof jQuery)? container : this.$(container);
        var margin = 0;
        if(orientation == 'horizontal') {
            margin = parseInt($obj.css('marginLeft'));
            $container.scrollLeft($obj.offset().left - $container.offset().left + $container.scrollLeft() - margin);
        }
        else {
            margin = parseInt($obj.css('marginTop'));
            $container.scrollTop($obj.offset().top - $container.offset().top + $container.scrollTop() - margin);
        }
        
        if(autoFocus) {
            this.blur($obj);
            this.focus($obj);
        }
    },
    
    
    // show/hide the next and prev buttons based on the pagination
    pagination: function(container, prevbutton, nextbutton, orientation, selector) {
        selector = selector || this.itemSelector;
        orientation = orientation || 'vertical';
        var $container = (container instanceof jQuery)? container : this.$(container);
        var $prev = (prevbutton instanceof jQuery)? prevbutton : this.$(prevbutton);
        var $next = (nextbutton instanceof jQuery)? nextbutton : this.$(nextbutton);
        var $items = $container.find(selector);
        if($items.length < 1)   // empty list
            return;
            
        var $first = $($items.get(0));
        var $last = $($items.get(-1));
        var pos = 0, pageFirst = false, pageLast = false;
        if(orientation == 'horizontal') {
            pos = $container.scrollLeft();
            pageFirst = ($first.position().left + $first.width()) > 0;
            pageLast = ($last.position().left + $last.width()) <= $container.width();
        }
        else {
            pos = $container.scrollTop();
            pageFirst = ($first.position().top + $first.height()) > 0;
            pageLast = ($last.position().top + $last.height()) <= $container.height();
        }
        if(pageFirst)
            $prev.hide();
        else
            $prev.show();
        
        if(pageLast)
            $next.hide();
        else
            $next.show();
    },
    
    translate: function(lang, key) {
        var context = this;
        var dict = this._getDictionary(lang);
        if(!dict)
            return null; // no dictionary, do nothing
        
        if(key) {
            // translate just a string
            if(dict[key])
                return dict[key];
        }
        else {
            // translate the entire page
            this.$('[data-translate]').each(function(i){
                var key = $(this).attr('data-translate');
                var text = context.translate(lang, key);
                if(text) {
                    $(this).contents().filter(function(){
                        return (this.nodeType == 3);
                    }).replaceWith(text);
                }
            });
        }
    },
   
    // private functions
    
    _track: function() {
        // Analytics
        if(this.trackPageView) {
            var title = this.pagePath;
            if(this.label) {
                title = this.label;
            }
            document.title = title;
            
            nova.tracker.pageView(this.pagePath, title, this.trackOptions);
        }
    },
    
    _ready: function($wrapper) {
        $wrapper.addClass('view-wrapper');
        
        this.$el = $wrapper.find('#' + this.id);
        if(this.$el.length<=0) {
            $('#' + this.wrapper).append('<div id="' + this.id + '"></div>');
            this.$el = $('#' + this.wrapper + ' #' + this.id);
        }
                
        if(this.className)
            this.$el.addClass(this.className);
        
        // callback to overwritten
        this.renderData();
        
        // translation happens after renderData()?    
        var lang = window.settings.language;
        if(lang && lang!='en') {
            this.translate(lang);
        }
        
        this.$el.show();
        
        // add into DOM
        var $container = $('#' + this.wrapper);
        if($container.length<=0) {
            $wrapper.hide();
            $('body').append($wrapper);
            $wrapper.show();  // change this if running on low-end device. We animate to reduce the paint effect.
        }
        else {
            $container.html($wrapper.html());
        }
        
        // add to global stack
        window.pages.push(this);
        
        // update cache
        this.$el = $('#' + this.wrapper + ' #' + this.id);
        
        this._addKeyListener();
        this._addMouseListener();
        
        // callback to overwritten
        this.shown();
        
        if(this.oncreate)
            this.oncreate();
        
    },
    
    // Hijack the global keypressed() function
    _addKeyListener: function() {
        this.original_keypressed = window.keypressed;
        var context = this;
        window.keypressed = function(keyCode) {
            var event = {};
            var processed = context._keypressed(keyCode, event);
            if(!processed && context.original_keypressed && !event.stopBubble)
                return context.original_keypressed(keyCode);
            return processed;
        };
    },
    
    // Restore the key handler
    _removeKeyListener: function() {
        window.keypressed = this.original_keypressed;
    },
    
    _keypressed: function(keyCode, e) {
        var akey = getkeys(keyCode);
        var $curr = this.$('a.active').filter(':visible');
        
        var handled = this.navigate(akey, e);
        if(window['msg'])   
            msg('( #' + this.id + ' ) keypress: code = ' + keyCode + ' - ' + akey + ' handled:' + handled  );
            
        // fall back to original handler if we ignore it
        if(!handled && !e.stopBubble) {
            
            if(akey == 'CLOSE') {   // just close this page
                this.destroy();
                return true;
            }
            else if(akey == 'CLOSEALL' || akey == 'MENU' || akey == 'HOME') {   // close page and bubble up
                this.destroy(true);
                return false;
            }
            else if(akey == 'POWR') {
                this.destroy();
                var version = window.settings.version;
                if(version == 'NEBULA') {   
                    var player = Nebula.getTVPlayer();
                    player.stop(false); // turn off tuner when display is off
                }
                return false;
            }
            else if(akey == 'ENTER' && $curr.length > 0) {  // default link click
                this.blur();
                this.focus($curr);
                return this.click($($curr));
            }
            
            return false;
        }
        else {
            return handled;
        }
        
        return false;
    },
    
    _addMouseListener: function() {
        var context = this;
        this.$el.delegate(this.itemSelector, 'click', function(e){
            context.blur($(this));
            context.focus($(this));
            var processed = context.click($(this));
            e.preventDefault();
        });
    },
    
    _removeMouseListener: function() {
        this.$el.undelegate(this.itemSelector, 'click');
    },
    
    _getDictionary: function(lang) {
        window.settings.dictionaries = window.settings.dictionaries || {};
        var dict = window.settings.dictionaries[lang];
        
        if(!dict) {
            var url = 'translation/' + lang + '.json';
            msg('get dictionary ' + lang + '.json');
            var file = ajaxJSON(url, '');
            if(file.dictionary) {
                window.settings.dictionaries[lang] = dict = file.dictionary;
            }
        }
        return dict;
    }
});

