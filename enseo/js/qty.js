var qty = View.extend({

    id: 'qty',
    
    template: 'qty.html',
    
    css: 'qty.css',
    
    trackPageView: false,
    
    /**********************************************************************************
     * Overwrite functions; from base class
     *********************************************************************************/

    navigate: function(key)	{
    	if(key == 'LEFT' || key == 'RIGHT') {
    	    this.changeFocus(key, '#buttons', 'a.button');
    	    return true;
    	}
    	return false;
    },
    
    // When "ENTER" is pressed on a link, or when link is clicked by mouse or touch screen
    click: function($jqobj) {
    	var linkid = $jqobj.attr('id');
        if(linkid == 'up') { // back button
    		
    		return false;
    	} 
		if(linkid == 'down') { // back button
    		
    		return false;
    	} 
    	
		
        if($jqobj.hasClass('close')) { // back button
    		this.destroy();
    		return true;
    	} 
   
		if(linkid == 'exit') {			
		    keypressed('CLOSEALL');
            keypressed(216);    //force going back to main menu

		    return false;
        }
    		
    	return false;
    },
    
    renderData: function() {
        var context = this;
        var data = this.data;
		
//        if(data.text1)
  //          this.$('#content #text1').html(data.text1);
    },
    
    /**********************************************************************************
     * Own public functions; Outside can call these functions
     *********************************************************************************/
    
    /**********************************************************************************
     * Private functions; Starts with '_' only used internally in this class
     *********************************************************************************/
    
});    