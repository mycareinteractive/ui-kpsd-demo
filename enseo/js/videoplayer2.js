// Video player 2 is a player ideal for playing a single internet video, or a playlist.
// It is different from the videoplayer.js because it is not doing SeaChange RTSP orderring
var VideoPlayer2 = View.extend({

    id: 'videoplayer2',
    
    template: 'videoplayer2.html',
    
    css: 'videoplayer2.css',
    
    // below is for single video playing, ideal for playing Multicast, http...etc 
    type: '', // 'Analog', 'Digital', 'UDP', 'HTTP'
    url: '', // '2', '57000:1', 'udp://@239.225.1.100:8000', 'http://sample.com/test.mp4'
    display: '', // 'CBS', 'CBS HD', 'Care Channel', 'Podcast 1'
    
    // below is for playlist playing.  If playlist is present, type and url are ignored
    // playlist is an array, each item has type, url and display
    playlist: null,
    playlistIndex: -1,
    
    delay: 0,
    delayMessage: '',
    
    // progress control
    seconds: 0.0,
    duration: 0.0,
        
    windowMode: true,
    windowModeX: 46,
    windowModeY: 0,
    windowModeWidth: 1188,
    windowModeHeight: 668,
    fullscreenX: 0,
    fullscreenY: 0,
    fullscreenWidth: 1280,
    fullscreenHeight: 720,
    
    /**********************************************************************************
     * Overwrite functions; from base class
     *********************************************************************************/

    navigate: function(key)	{
    	var handled = true;
        
        switch(key) {
            case 'VIDEO': // toggle between fullscreen and windowed TV mode
                this.toggleWindowMode();
                break;
            case 'STOP':
                this.destroy();
                break;
            case 'CHUP':
                this.prevVideo();
                break;
            case 'CHDN':
                this.nextVideo();
                break;
            case 'LEFT':
            case 'RIGHT':
                this.changeFocus(key, '#controlbar #buttons');
                break;
            default:
                handled = false;
                break;
        }
        
        return handled;
    },
    
    click: function($jqobj) {
    	var linkid = $jqobj.attr('id');
        var parentid = $jqobj.parent().attr('id');
        	
    	if(linkid == 'stop') { // stop button
    		this.destroy();
    		return true;
    	}
    	else if(linkid == 'fullscreen') {   // full screen button
            this.setWindowMode(false);
            return true;
        }
    	else if(linkid == 'exitfullscreen') { // exit full screen
            this.setWindowMode(true);
            return true;
        }

    },
    
    renderData: function() {
        msg('videoplayer2 renderData');
        var context = this;
        
        if(this.delay > 0) {
            context.$('#mask #masktext').text(this.delayMessage);
            context.$('#mask').show();
            $.doTimeout('videoplayer2 delay', context.delay, function() {
                context.$('#mask').hide();
                return false;
            });
        }
        else {
            context.$('#mask').hide();
        }
    },
    
    shown: function() {
		var context = this;
        
        this.startVideo();
        
        var $firstObj = this.$('#buttons a:first');
        this.focus($firstObj);
    },
    
    uninit: function() {
        // stop player
        this.stopVideo();
        
        // stop clock
        $.doTimeout('video npt'); 
    },
    
    /**********************************************************************************
     * Own public functions; Outside can call these functions
     *********************************************************************************/
    startVideo: function() {
        msg('videoplayer2 startVideo');
        
        var type = this.type;
        var url = this.url;
        var display = this.display;
        
        if($.isArray(this.playlist)) {
            var item = this.playlist[this.playlistIndex];
            if(item) {
                type = item.type;
                url = item.url;
                display = item.display;
            }
        }
        
        if(!type || !url) {
            msg('videoplayer2 Invalid type or url');
            return;
        }
        
        var videoUrl = url;
        if(type.toLowerCase() == 'html5') {
	        if(videoUrl.indexOf('youtube.com')>0)
	            this.html5VideoType = 'youtube';
	        else if(videoUrl.indexOf('vimeo.com')>0)
	            this.html5VideoType = 'vimeo';
	        else
	            this.html5VideoType = videoUrl.substr(videoUrl.lastIndexOf('.')+1);
    	}
        else {
        	this.html5VideoType = null;
        }
    
        this.$('#progress p.title').text(display);
                
        var version = window.settings.version;
        if(this.html5VideoType) {
            var player = Nebula.getHtml5Player();
            player.play(this.$('#videopanel'), [{src:videoUrl, type:'video/'+this.html5VideoType}]);
            this.setWindowMode(this.windowMode);
        }
        else if(version == 'NEBULA') {
            var player = Nebula.getTVPlayer();
            player.play(type, url, display);
            player.setTVLayerEnable(true);
            this.setWindowMode(this.windowMode);
        } 
        
    },
    
    stopVideo: function() {
        msg('videoplayer2 stopVideo');
        
        var version = window.settings.version;
        if(this.html5VideoType) {
            var player = Nebula.getHtml5Player();
            player.stop();
        }
        else if(version == 'NEBULA') {
            var player = Nebula.getTVPlayer();
            player.setTVLayerEnable(false);
            player.stop();
        }
    },
    
    nextVideo: function() {
        if($.isArray(this.playlist)) {
            this.playlistIndex++;
            this.playlistIndex = (this.playlistIndex + this.playlist.length) % this.playlist.length;
            this.startVideo();
        }
    },
    
    prevVideo: function() {
        if($.isArray(this.playlist)) {
            this.playlistIndex--;
            this.playlistIndex = (this.playlistIndex + this.playlist.length) % this.playlist.length;
            this.startVideo();
        }
    },
    
    toggleWindowMode: function() {
        msg('toggleWindowMode');
        if(this.windowMode) { 
            // go fullscreen
            this.setWindowMode(false);
        }
        else {
            // shrink to window mode, and update EPG info
            this.setWindowMode(true);
        }
    },
    
    setWindowMode: function(windowMode) {
		var platform = window.settings.version;
        
        this.windowMode = windowMode;
        if(!this.windowMode) { 
            // fullscreen
            this.$('#videopanel').removeClass('windowmode').addClass('fullscreen');
            this.$('#exitfullscreen').show();
            if(this.html5VideoType) {
                var player = Nebula.getHtml5Player();
                if (player) {
                    player.setVideoLayerRect(this.fullscreenX, this.fullscreenY, this.fullscreenWidth, this.fullscreenHeight);
                }
            }
            else {
                if(platform == 'NEBULA') {
                    var player = Nebula.getRtspClient();
                    if (player)
                        player.setTVLayerRect(this.fullscreenX, this.fullscreenY, this.fullscreenWidth, this.fullscreenHeight);
                }
                else if(platform == 'ENSEO') {
                    var player = Nimbus.getPlayer();
                    if (player)
                        player.setVideoLayerRect(this.fullscreenX, this.fullscreenY, this.fullscreenWidth, this.fullscreenHeight);
                }
            }
        }
        else {
            // window mode
            this.$('#videopanel').removeClass('fullscreen').addClass('windowmode');
            this.$('#exitfullscreen').hide();
            if(this.html5VideoType) {
                var player = Nebula.getHtml5Player();
                if (player) {
                    player.setVideoLayerRect(this.windowModeX, this.windowModeY, this.windowModeWidth, this.windowModeHeight);
                }
            }
            else {
                if(platform == 'NEBULA') {
                    var player = Nebula.getRtspClient();
                    if (player)
                        player.setTVLayerRect(this.windowModeX, this.windowModeY, this.windowModeWidth, this.windowModeHeight);
                }
                else if(platform == 'ENSEO') {
                    var player = Nimbus.getPlayer();
                    if (player)
                        player.setVideoLayerRect(this.windowModeX, this.windowModeY, this.windowModeWidth, this.windowModeHeight);
                }
            }
        }
    },
    
    /**********************************************************************************
     * Private functions; Starts with '_' only used internally in this class
     *********************************************************************************/
     _updateCurrent: function() {
        var current = this._calcDuration(this.seconds);
        this.$('#progress p.current').text(current);
    },
    
    _calcDuration: function (duration) {
        if(!duration)
            return '0:00:00';
        
        duration = Math.floor(duration);
        
        var str = '';
        var h = Math.floor(duration/3600);
        str += h + ':';
        duration = duration%3600;
        
        var mm = Math.floor(duration/60);
        str += (mm<10?'0':'') + mm + ':';
        duration = duration%60;
        
        var ss = duration;
        str += (ss<10?'0':'') + ss;
        return str;
    }
});    