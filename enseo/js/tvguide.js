var TVGuide = View.extend({

    id: 'tvguide',
    
    template: 'tvguide.html',
    
    css: 'tvguide.css',
    
    channels: null,
    channelIndex: -1,
    dateBeg: null,
    dateEnd: null,
    dateNow: null,
    
    windowMode: true,
    windowModeX: 985,
    windowModeY: 25,
    windowModeWidth: 328,
    windowModeHeight: 185,
    fullscreenX: 0,
    fullscreenY: 0,
    fullscreenWidth: 1366,
    fullscreenHeight: 768,

    /**********************************************************************************
     * Overwrite functions; from base class
     *********************************************************************************/

    navigate: function(key) {
        var handled = false;
        
        switch(key) {
            case 'UP':
            case 'DOWN':
                handled = true;
                break;
            case 'LEFT':
            case 'RIGHT':
                handled = true;
                break;
            case 'VIDEO': // toggle between fullscreen and windowed TV mode
                this.toggleWindowMode();
                handled = true;
                break;
            case 'CHUP':
                if(!this.windowMode)
                    this.switchChannel(1);
                handled = true;
                break;
            case 'CHDN':
                if(!this.windowMode)
                    this.switchChannel(-1);
                handled = true;
                break;
            case 'STOP':
                this.destroy();
                handled = true;
                break;
            default:
                handled = false;
                break;
        }
        
        return handled;
    },
    
    // When "ENTER" is pressed on a link, or when link is clicked by mouse or touch screen
    click: function($jqobj) {
        var linkid = $jqobj.attr('id');
        msg('click: id=' + linkid);
        if($jqobj.hasClass('back')) { // back button
            this.destroy();
            return true;
        } 
        
        if($jqobj.hasClass('program')) { // clicking a program updates the info section 
            this._updateInfo($jqobj);
            this.playChannelByIdx(this.channelIndex);
            return true;
        }
        
        if($jqobj.hasClass('channelname')) { // clicking a channel tunes to the channel
            this._updateChannelInfo($jqobj);
            this.playChannelByIdx(this.channelIndex);
            return true;
        }
        
        if(linkid == 'fullscreen') {   // full screen text button
            this.setWindowMode(false);
            return true;
        }
        
        if(linkid == 'tvpanel') {  // just in case the native TV player window is gone, we fallback to the containing HTML div
            //this.toggleWindowMode();
            return true;
        }
        
        if(linkid == 'next' || linkid == 'prev') {
            this._stepEpg(linkid);
            return true;
        } 
        
        return false;
    },
    
    focus: function($jqobj) {
        if($jqobj.hasClass('program')) {
            this._super($jqobj);
            var chId = $jqobj.parent().attr('id');
            this.$('#gridpanel #content .channelstation #'+chId).addClass('active');
        }
        else if($jqobj.hasClass('channelname')) {
            this._super($jqobj);
        }
    },
    
    blur: function($jqobj) {
        this.$('.program,.channelname').removeClass('active');
        this._super($jqobj);
    },
    
    renderData: function() {
        var context = this;
        this._buildEpg();
        this.playChannelByIdx(this.channelIndex);
        
        this.$('#content').delegate('.program,.channelname', 'tap', function(e) {
            context.blur($(this));
            context.focus($(this));
            context.click($(this));
        });
        
        var context = this;
        context.$('#mask').hide();
        $.doTimeout('tv tuner init', 10000, function() {
            context.$('#mask').hide();
            return false;
        });
    },
    
    shown: function() {
        window.setTimeout(function(){
            this.iScroll = new IScroll(this.$('#gridpanel #content')[0], {
                mouseWheel: true,
                scrollbars: 'custom',
                interactiveScrollbars: true,
                tap: true,
                momentum: false,
                shrinkScrollbars: 'clip'
            });
        }, 500);
    },
    
    uninit: function() {
        // remove EPG clicking events
        this.$('#content').undelegate('.program,.channelname', 'tap');
        // stop clock
        $.doTimeout('tvguide clock'); 
        // stop player
        this.stopChannel();
    },
    
    /**********************************************************************************
     * Own public functions; Outside can call these functions
     *********************************************************************************/
    toggleWindowMode: function() {
        msg('toggleWindowMode');
        if(this.windowMode) { 
            // go fullscreen
            this.setWindowMode(false);
        }
        else {
            // shrink to window mode, and update EPG info
            this.setWindowMode(true);
            this._buildEpg();
        }
    },
    
    setWindowMode: function(windowMode) {
        var platform = $("#K_version").text(); 
        
        this.windowMode = windowMode;
        if(!this.windowMode) { 
            // fullscreen
            this.$('#tvpanel').removeClass('windowmode').addClass('fullscreen');
            
            if(platform == 'NEBULA') {
                var player = Nebula.getTVPlayer();
                player.setTVLayerRect(this.fullscreenX, this.fullscreenY, this.fullscreenWidth, this.fullscreenHeight);
            }
            else if(platform == 'ENSEO') {
                var player = Nimbus.getPlayer();
                if (player)
                    player.setVideoLayerRect(this.fullscreenX, this.fullscreenY, this.fullscreenWidth, this.fullscreenHeight);
            }
        }
        else {
            // window mode
            this.$('#tvpanel').removeClass('fullscreen').addClass('windowmode');
            
            if(platform == 'NEBULA') {
                var player = Nebula.getTVPlayer();
                player.setTVLayerRect(this.windowModeX, this.windowModeY, this.windowModeWidth, this.windowModeHeight);
            }
            else if(platform == 'ENSEO') {
                var player = Nimbus.getPlayer();
                if (player)
                    player.setVideoLayerRect(this.windowModeX, this.windowModeY, this.windowModeWidth, this.windowModeHeight);
            }
        }
    },
    
    playChannelByIdx: function(idx) {
        idx = idx || this.channelIndex;
        if(!this.channels || this.channels.length<1) {
            Ion.log('Channel list is empty, nothing to play.');
            return;
        }
        
        if(idx < 0 || idx >= this.channels.length) {
            Ion.log('Channel index ' + idx + ' out of bound.');
            return;
        }
        
        var platform = $("#K_version").text(); 
        var ch = this.channels[idx];
        var chId = ch.channelID, chNum = ch.channelNumber, chName = ch.channelName, freq = ch.frequency, pn = ch.programNumber;
        var pnInt = parseInt(pn);
        
        var isDigital = true;
        var isIP = false;
        
        if (isNaN(pnInt) || pnInt <= 0) {
            isDigital = false;
        }
        
        msg('Play channel num=' + chNum + ', freq=' + freq + ', pn=' + pn + ', name=' + chName + ', ip?' + isIP + ', digital?' + isDigital);
        if(platform == 'NEBULA') {  
            var player = Nebula.getTVPlayer();
            this.setWindowMode(this.windowMode);
            player.setTVLayerEnable(true);
            if(!isIP && !isDigital) {   // Nebula only support analog channel now
                player.play('Analog', chNum, chName);
            }
        }
        else if(platform == 'ENSEO') {
            var parm1 = 'PhysicalChannelIDType=Freq';
            var parm2 = 'PhysicalChannelID="'+freq+'"';
            var parm3 = 'DemodMode="QAMAuto"';
            var parm4 = 'ProgramSelectionMode="PATProgram"';
            var parm5 = 'ProgramID="'+pn+'"';
            var url = '';
            if(isDigital) {
                var parms = ' ' + parm1 + ' ' + parm2 + ' ' + parm3 + ' ' + parm4 + ' ' + parm5 + ' ';
                url = '<ChannelParams ChannelType="Digital"><DigitalChannelParams'+parms+'></DigitalChannelParams></ChannelParams>';
            }
            else {
                var parms = ' '+parm1+' '+parm2+' ';
                url = '<ChannelParams ChannelType="Analog"><AnalogChannelParams'+parms+'></AnalogChannelParams></ChannelParams>';
            }
            var sessionID = getSessionID();
            var player = Nimbus.getPlayer();
            if (player) {
                player.stop();
                player.close();
            }
            var player = Nimbus.getPlayer( url, null, sessionID );
            if (player) {
                player.setChromaKeyColor(0x00000000);
                player.setVideoLayerBlendingMode("colorkey");
                player.setVideoLayerTransparency(1);
                player.setPictureFormat("Widescreen");
                player.setVideoLayerEnable(true);
                player.play();
            }
        }
    },
    
    stopChannel: function() {
        var platform = $("#K_version").text();
        if(platform == 'NEBULA') {  
            var player = Nebula.getTVPlayer();
            if(player) {
                player.stop();
                player.setTVLayerEnable(false);
            }
        }
        else if(platform == 'ENSEO') {
            var player = Nimbus.getPlayer();
            if (player) {
                player.stop();
                player.close();
                player.setVideoLayerEnable(false);
            }
        }
    },
    
    switchChannel: function(offset) {
        var len = this.channels.length;
        this.channelIndex += offset;
        this.channelIndex = this.channelIndex<0? this.channelIndex+len: this.channelIndex;
        this.channelIndex = this.channelIndex % len;
        
        this.playChannelByIdx(this.channelIndex);
    },
    
    /**********************************************************************************
     * Private functions; Starts with '_' only used internally in this class
     *********************************************************************************/
    _buildEpg: function(type)    {
        if (type=='scenictv')   {
            this._scenicTV(); 
            return;
        }
        
        this._resetTimes();
        this._cleanAll();
        this._buildHeaders();
        this._buildChannels();
        this._buildPrograms();
        this._buildInfo();
        this._focusProgram();
        
        return;
    },
    
    _stepEpg: function(direction) {
        if (direction == 'next')
            this._stepTimes(2);
        else if (direction == 'prev')
            this._stepTimes(-2);
        else
            return;
        
        this._cleanPrograms();
        this._buildHeaders();
        this._buildPrograms();   
    },
    
    _buildHeaders: function() {
        this.$('#gridpanel #header #today').text(this.dateNow.format('ddd m/d', false, window.settings.language));
        this.$('#gridpanel #header div.header1').text(this.dateBeg.format('h:MM TT'));
        this.$('#gridpanel #header div.header2').text(this.dateBeg.addMinutes(30).format('h:MM TT'));
        this.$('#gridpanel #header div.header3').text(this.dateBeg.addMinutes(60).format('h:MM TT'));
        this.$('#gridpanel #header div.header4').text(this.dateBeg.addMinutes(90).format('h:MM TT'));
    },
    
    _buildChannels: function() {
        var context = this;
        this.channels = epgChannels().Channels;
        $.each(this.channels, function(i, ch) {
            var $li = $('<li></li>');
            var $channel = $('<div></div>').addClass('channelstation');
            var $name = $('<div class="channelname"></div>').attr('id', ch.channelID).html('<span>'+ch.channelNumber + ' ' + ch.channelName+'</span>');
            $li.append($channel.append($name)).appendTo(context.$('#gridpanel #program'));
        });
    },
    
    _buildPrograms: function() {
        var context = this;
        var filename = this.dateBeg.format('yyyymmddHHMM') + '.txt';
        var epgfile = this._getProgramFile(filename);
        $.each(this.channels, function(i, ch) {
            var $c = $('<div class="channel"></div>').attr('id', ch.channelID);
            var hasData = false;
            if(epgfile.ListOfChannel[ch.channelID]) {         
                $.each(epgfile.ListOfChannel[ch.channelID], function(i,row){
                    if (row["programName"]) {
                        hasData = true;
                        var d = context._calcDuration(row["startTime"],row["endTime"]);
                        if (d.duration>0)   {
                            var $pname = $('<span></span>').text(row.programName);
                            $('<div class="program"></div>').attr('id', row.programID).attr('title', row.programName).append($pname)
                                .attr('data-begin', d.begdate.getTime()).attr('data-end', d.enddate.getTime()).attr('data-duration', d.duration)
                                .addClass('D'+d.duration).appendTo($c);
                        }
                    }
                });
            }
            
            if(!hasData) {  // if no program, by default just fill in the channel name
                var $pname = $('<span></span>').text(ch.channelName);
                $('<div id="na" class="program D120"></div>').attr('title', ch.channelName).append($pname).appendTo($c);
            }
            
            context.$('#gridpanel #program #' + ch.channelID).parent().parent().append($c);
        });
    },
    
    _buildInfo: function() {
        // start clock
        var context = this;
        $.doTimeout('tvguide clock', 60000, function() {
            var d = new Date();
            var format = 'dddd, mmm d | h:MM TT';
            var locale = window.settings.language;
            context.$('#gridinfo #current p.date').text(d.format(format, false, locale));
            return true;
        });
        $.doTimeout('tvguide clock', true); // do it now
    },
    
    _updateInfo: function($obj) {
        if(!$obj) {
            this.$('#gridinfo #current p.channel').empty();
            this.$('#gridinfo #current p.title').empty();
            this.$('#gridinfo #current p.time').empty();
            return;
        }
        
        // update channel info
        var chId = $obj.parent().attr('id');
        var $ch = this.$('#gridpanel #content .channelstation #'+chId);
        this._updateChannelInfo($ch);
        
        // update program info
        var name = $obj.text();
        var id = $obj.attr('id');
        var beg = new Date($obj.attr('data-begin')*1);
        var duration = $obj.attr('data-duration');

        if(id=='na') {
            this.$('#gridinfo #current p.title').text(name);
            this.$('#gridinfo #current p.time').empty();
        }
        else {
            this.$('#gridinfo #current p.title').text(name);
            this.$('#gridinfo #current p.time').text(beg.format('h:MMtt ') + duration + 'min');
        }
    },
    
    _updateChannelInfo: function($obj) {
        if(!$obj || $obj.length <1)
            return;
            
        this.$('#gridinfo #current p.title').empty();
        this.$('#gridinfo #current p.time').empty();
        
        this.channelIndex = $obj.index();
        var chName = $obj.find('span').text();
        this.$('#gridinfo #current p.channel').text(chName);
    },
    
    _focusProgram: function() {
        var context = this;
        this.channelIndex = this.channelIndex<0? 0: this.channelIndex;
        if(this.channelIndex >= 0 && this.channelIndex < this.channels.length) {
            var ch = this.channels[this.channelIndex];
            var $ch = context.$('#gridpanel #content #program #' + ch.channelID);
            context.$('#gridpanel #content .channelstation #'+ch.channelID).addClass('active');
            $ch.find('.program').each(function(i){
                var id = $(this).attr('id');
                var beg = new Date($(this).attr('data-begin')*1);
                var end = new Date($(this).attr('data-end')*1);
                if(id=='na' || (context.dateNow.getTime() >= beg.getTime() && context.dateNow.getTime() <= end.getTime())) {
                    $(this).addClass('active');
                    context._updateInfo($(this));
                    return false;
                }    
            });
            context.$('#gridpanel #content').scrollTop($ch.position().top - 64*3);
        }
    },
    
    _getProgramFile: function(filename) {
        var epgname = this.cachedEpgName;
        var epgfile = this.cachedEpgFile;
        if(!epgname || !epgfile || epgname!=filename) {
            var ewf     = ewfObject();
            var url     = ewf.epgFilePath+filename;
            var args    = '';
            epgfile = ajaxJSON(url,args);
            if(epgfile) {
               this.cachedEpgName = filename;
               this.cachedEpgFile = epgfile;
            } 
        }
        else {
            msg('Using cached epg file ' + filename);
        }
        return epgfile;
    },
    
    _cleanAll: function()  {
        this.$('#gridinfo #current p.channel').empty();
        this.$('#gridinfo #current p.title').empty();
        this.$('#gridinfo #current p.time').empty();
        this.$("#gridpanel #header div").empty();
        this.$("#gridpanel #program").empty();
    },
    
    _cleanPrograms: function() {
        this.$("#gridpanel #program .channel").remove();
        this.$('#gridinfo #current p.title').empty();
        this.$('#gridinfo #current p.time').empty(); 
    },
    
    // set EPG window to current
    _resetTimes: function() {
        this.dateNow = new Date();
        var d = new Date(this.dateNow);
        d.setMilliseconds(0);
        d.setSeconds(0);
        d.setMinutes(0);
        this.dateBeg = new Date(d);
        this.dateEnd = new Date(d);
        
        var h = this.dateNow.getHours();
        this.dateBeg.setHours(Math.floor(h/2)*2);
        this.dateEnd.setHours(Math.floor(h/2)*2+2);
    },
    
    // step begin and end time of the EPG window
    _stepTimes: function(hours) {
        this.dateBeg = this.dateBeg.addHours(hours);
        this.dateEnd = this.dateEnd.addHours(hours);
    },
    
    // parse EPG time format '2013-05-30 13:14:00.0'
    _parseEpgTime: function(str){
        var dateTime = str.split(" ");

        var date = dateTime[0].split("-");
        var yyyy = date[0];
        var mm = date[1]-1;
        var dd = date[2];
    
        var time = dateTime[1].split(":");
        var h = time[0];
        var m = time[1];
        var s = parseInt(time[2]); //get rid of that 00.0;
    
        return new Date(yyyy,mm,dd,h,m,s);
    },
    
    // adjust time to nearest 30min slot
    _roundUpTime: function(datetime) {
        var d = new Date(datetime);
        var m = (Math.round(d.getMinutes()/30) * 30) % 60;
        var h = (d.getMinutes()>=45)? d.getHours()+1 : d.getHours();
        d.setMilliseconds(0);
        d.setSeconds(0);
        d.setMinutes(m);
        d.setHours(h);
        return d;
    },
    
    // calculate begin, time and duration for specific program
    _calcDuration: function(startTime,endTime) {
        var duration = 0;
        var beg = this._parseEpgTime(startTime);
        var end = this._parseEpgTime(endTime);
        
        beg = this._roundUpTime(beg);
        end = this._roundUpTime(end);
        
        // adjust program if it begins earlier or end later than current epg window
        if(beg.getTime()<this.dateBeg.getTime())    
            beg = this.dateBeg;
        if(end.getTime()>this.dateEnd.getTime())
            end = this.dateEnd;
        
        var diff = (end.getTime()-beg.getTime())/1000/60;
        diff = Math.floor(diff/30)*30;
        duration = diff>0? diff: 0;
        
        var ret = Array();
        ret['duration'] = duration;
        ret['begdate']  = beg;
        ret['enddate']  = end;
        return ret;
    }
});    