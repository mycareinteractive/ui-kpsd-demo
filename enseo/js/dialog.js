var Dialog = View.extend({
    
    id: 'dialog',
    
    template: 'dialog.html',
    
    css: 'dialog.css',
    
    trackPageView: false,
    
    message: '',
    
    icon: '',
    
    header: '',
    
    /**********************************************************************************
     * Overwrite functions; from base class
     *********************************************************************************/
    navigate: function(key)    {
        return false;   // not support keyboard
    },
    
    click: function($jqobj) {
        return false;
    },

    renderData: function() {
        var context = this;
        
        this.$('#close').click(function(e){
            context.destroy();
            e.preventDefault();
        });
        
        this.$('#header').html(this.header);
        this.$('#icon').removeClass().addClass(this.icon);
        this.$('#message').html(this.message);
    },
    
    /**********************************************************************************
     * Own public functions; Outside can call these functions
     *********************************************************************************/
    
    icon: function(icon) {
        this.icon = icon;
    },
    
    message: function(msg) {
        this.message = msg;
    }
});



