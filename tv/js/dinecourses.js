var dinecourses = View.extend({
    
    id: 'dinecourses',
    
    template: 'dinecourses.html',
    
    css: 'dinecourses.css',
    
    /**********************************************************************************
     * Overwrite functions; from base class
     *********************************************************************************/
    navigate: function(key)    {
        var navkeys = ['LEFT','RIGHT'];
        if(navkeys.indexOf(key)!=-1) {
            this.changeFocus(key);
            return true;
        }
        
        return false;
    },
    
    click: function($jqobj) {
			var currLabel = this.data.label
			var linkid = $jqobj.attr('id');
			var coursemax = $jqobj.attr('max');
			var coursename = $jqobj.attr('title');
msg(coursename);
        if(linkid == 'exit') {			
		    keypressed('CLOSEALL');
            keypressed(216);    //force going back to main menu
			$.doTimeout('dinecourse clock'); // stop clock
			this.destroy();
            return false;
        }
		
		if(linkid == 'cancel' ) {

				var data = {
    	           text1: '<p>Warning</p><br/><br><br><br><p>Any orders that<br/>have not been placed<br/>will be canceled.<br/><br/>Would you like to cancel this order?</p>',
    	       };
			
    	       var page = new warning({className:'dinedecline', data: data});
    	       page.render();
			   return true;

		}
	
        if(linkid == 'back' ) {
            this.destroy();
            return true;
        }
		else if(linkid == 'view') {
			if(window.meals.length>=1) {				
                var page = new dineview({viewId: 'dineview', breadcrumb:currLabel, data: ''});
                page.render();                
			} else {
            	var data = {
    	           text1: '<p>No Items Selected<br/><br/></p><p><span>Please make your selections<br/> and then view your order', 
    	       };
    	       var page = new Information({className:'dinedecline', data: data});
    	       page.render();
            }
			return true;
        }
		
		else if(linkid == 'help') {
				this.$('#mask').hide();
    	       var data = {
    	           text1: '<p>Need Help?<br/><br/><br/></p><p><span>Please call your Food and Nutrition Team <br/>at Ext. 24-3463</span></p>', 
    	       };
    	       var page = new Information({className:'dinedecline', data: data});
    	       page.render();								
               return true;
		}

		else if(coursename == 'Build Sandwich') {
				
	var context = this;
    var data = this.data;

	var coursestring = '';
	var mealname = '';
	window.settings.coursemax = coursemax;
	$(data).find("Person").each(function() { 
		$(data).find("Meals").each(function() { 
			$(data).find("Meal").each(function() { 
				$(data).find("ServiceCourse").each(function() { 
					var id = $(this).attr("Id");
				var subdata = $(this);
				if(id == linkid) {
					var page = new dinesandwich({className:linkid, breadcrumb:"dinesandwich", data: subdata
					});
					page.render();
				}
				
				});
			});
		});
	});

				
				
            return true;
		}

		else if(coursename == 'Condiments') {
				
	var context = this;
    var data = this.data;

	var coursestring = '';
	var mealname = '';
	window.settings.coursemax = coursemax;
	$(data).find("Person").each(function() { 
		$(data).find("Meals").each(function() { 
			$(data).find("Meal").each(function() { 
				$(data).find("ServiceCourse").each(function() { 
					var id = $(this).attr("Id");
				var subdata = $(this);
				if(id == linkid) {
					var page = new dinecondiments({className:linkid, breadcrumb:"dinecondiments", data: subdata
					});
					page.render();
				}
				
				});
			});
		});
	});

				
				
            return true;
		}

		
		
		else  {
			
	var context = this;
    var data = this.data;

	var coursestring = '';
	var mealname = '';
	window.settings.coursemax = coursemax;
	$(data).find("Person").each(function() { 
		$(data).find("Meals").each(function() { 
			$(data).find("Meal").each(function() { 
				$(data).find("ServiceCourse").each(function() { 
					var id = $(this).attr("Id");
				var subdata = $(this);
				if(id == linkid) {
					var page = new dineitems({className:linkid, breadcrumb:"dinecourses", data: subdata
					});
					page.render();
				}
				
				});
			});
		});
	});


			
		}
        
    
        
        return false;
    },
    
    renderData: function() {
		window.meals = window.meals || new Array();

		var context = this;	
			
		
		
	$('#' + this.wrapper + " #dinecourses").addClass(this.classStr);
    var data = this.data;

	
	
		var maxcarbs = '';
	$(data).find("Person").each(function() { 
		$(data).find("Meals").each(function() { 
		$(data).find("Meal").each(function() { 
				$(data).find("Goal").each(function() { 
					$(data).find("GoalNutrient").each(function() { 

					var nutrientid = $(this).attr("NutrientId");
					
					if(nutrientid=='10101') {
								maxcarbs = $(this).attr("MaxValue");					
						}				
				});
			});
		});
	});
	});
	window.settings.maxcarbs = maxcarbs;
	// show the div
	$('#' + this.wrapper + " #dinecourses").show();
	
	var selections = this.$('#dinecourses #selections');
	var meals = Array();
	var coursestring = '';
	var mealname = '';
	$(data).find("Person").each(function() { 
		$(data).find("Meals").each(function() { 
			$(data).find("Meal").each(function() { 
					mealname = $(this).attr("Name");					
				$(data).find("ServiceCourse").each(function() { 

				var course = $(this).attr("Name");
				var max = $(this).attr("MaxSelection");
				var id = $(this).attr("Id");
				
				
						coursestring = coursestring + '<a href="#" title = "' + course  + '"class = "course ' + course + '" max="' + max + '" id="' + id + '">' + course + '</a>';
				});
			});
		});
	});

			this.$('#banner #meal').html(mealname);

		// start clock
        $.doTimeout('dinecourse clock', 60000, function() {
            var d = new Date();
            var format = 'dddd, mm.dd.yyyy - h:MM TT';
            var locale = window.settings.language;
            context.$('#banner #datetime').text(d.format(format, false, locale));
            return true;
        });
        $.doTimeout('dinecourse clock', true); // do it now

		this.$('#selections').html(coursestring);
    },
    
    /**********************************************************************************
     * Own public functions; Outside can call these functions
     *********************************************************************************/
    
    /**********************************************************************************
     * Private functions; Starts with '_' only used internally in this class
     *********************************************************************************/

});

