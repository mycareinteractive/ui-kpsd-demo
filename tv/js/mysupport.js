var MySupport = View.extend({

    id: 'mysupport',
    
    template: 'mysupport.html',
    
    css: 'mysupport.css',
    
    mrn: '',
    supportId: '',
    supportValue: '',
    /**********************************************************************************
     * Overwrite functions; from base class
     *********************************************************************************/

    navigate: function(key, e)	{
    	var specialKeys = [
            'ENTER', 'HOME', 'END', 'LEFT', 'RIGHT', 'UP', 'DOWN', 'PGUP', 'PGDN',
            '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
            'CHUP', 'CHDN' ];
            
        if(specialKeys.indexOf(key)>=0) {
            e.stopBubble = true;
            return false;
        }
        return false;
    },
    
    // When "ENTER" is pressed on a link, or when link is clicked by mouse or touch screen
    click: function($jqobj) {
    	var linkid = $jqobj.attr('id');
        
        if($jqobj.hasClass('back')) { // back button
    		this.destroy();
    		return true;
    	}
    	
    	if(linkid == 'save') { // save it
    	    if(!this._saveMySupport()) { // fail to save
                var page = new Dialog({message:'Sorry we are having problem saving your support person information.  Please try again later.'});
                page.render();
                return true;
            }
    	    this.destroy();
    	    return true;
    	}
    	
    	return false;
    },
    
    renderData: function() {
        this._getMRN();
        this._loadMySupport();
    },
    
    /**********************************************************************************
     * Own public functions; Outside can call these functions
     *********************************************************************************/
    
    /**********************************************************************************
     * Private functions; Starts with '_' only used internally in this class
     *********************************************************************************/
    _getMRN: function() {
        var ewf = ewfObject();
        var mrn, url, xml, dataobj;
    
        // Get MRN    
		var mrn = this.mrn;
		if(!mrn)
			mrn = getMRNDATA();		
		this.mrn = mrn;

        dataobj = {};
        dataobj['room'] = window.settings.room;
        dataobj['bed'] = window.settings.bed;
        dataobj['name'] = window.settings.userFullName;
    
        this.mrn = mrn;
    },
    
    _loadMySupport: function() {
        var context = this;
        var ewf = ewfObject();
        var url, xml, dataobj;
    
        // Get support person
        url = ewf.getclinical + "?type=mysupport&mrn=" + this.mrn + "&numrec=1&sortorder=asc"
        dataobj = '';
        xml  = getXdXML(url, dataobj);      

        $(xml).find("item").each(function() {
            context.supportId = $(this).find("id").text();
            context.supportValue = unescape($(this).find("value").text());         
        });
        
        if(!context.supportValue)
            return;
            
        var vals = context.supportValue.split('\t');
        var names = vals[0]? vals[0].split(' '): ['',''];
        var phone = vals[1];
        
        this.$('#firstname').val(names[0]||'');
        this.$('#lastname').val(names[1]||'');
        this.$('#phonenumber').val(phone||'');
    },
    
    _saveMySupport: function() {
        if(this.supportValue && !this.supportId) {
            // server returned value but not ID, don't save it
            return false;
        }
        
        var firstname = this.$('#firstname').val();
        var lastname = this.$('#lastname').val();
        var phonenumber = this.$('#phonenumber').val();
        
        var value = firstname + ' ' + lastname + '\t' + phonenumber;
            
        var xmlValue = '';
        xmlValue += '<?xml version="1.0" encoding="ISO-8859-1" ?>'; 
        xmlValue += '<clinicaldata>';
        xmlValue += '<id>' + (this.supportId||'') + '</id>';
        xmlValue += '<ptname>' + window.settings.userFullName + '</ptname>';
        xmlValue += '<mrn>' + this.mrn + '</mrn>'; 
        xmlValue += '<room>' + window.settings.room + '</room>';
        xmlValue += '<bed>' + window.settings.bed + '</bed>';
        xmlValue += '<type>mysupport</type>';
        xmlValue += '<value>'+ escape(value) + '</value>';
        xmlValue += '</clinicaldata>';
        
        var ewf = ewfObject();
        var url = ewf.addclinical;
        var ret = postXdXML(url, {patronMenu:xmlValue});
        
        return ret?true:false;
    }
});    