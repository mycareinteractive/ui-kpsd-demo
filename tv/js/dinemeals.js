var dinemeals = View.extend({
    
    id: 'dinemeals',
    
    template: 'dinemeals.html',
    
    css: 'dinemeals.css',
    
    /**********************************************************************************
     * Overwrite functions; from base class
     *********************************************************************************/
    navigate: function(key)    {
        var navkeys = ['LEFT','RIGHT'];
        if(navkeys.indexOf(key)!=-1) {
            this.changeFocus(key);
            return true;
        }
        
        return false;
    },
    
    click: function($jqobj) {
			var currLabel = this.data.label
			var linkid = $jqobj.attr('id');
			var alreadyselected = $jqobj.attr('alreadyselected');
			
        msg(linkid);
		if(linkid == 'exit') {			
		    keypressed('CLOSEALL');
            keypressed(216);    //force going back to main menu

			this.destroy();
            return false;
        }
        else if(linkid == 'help') {
				this.$('#mask').hide();
    	       var data = {
    	           text1: '<p>Need Help?<br/><br/><br/></p><p><span>Please call your Food and Nutrition Team <br/>at Ext. 24-3463</span></p>',    	         
    	       };
    	       var page = new Information({className:'dinedecline', data: data});
    	       page.render();
				
				
            return true;
		}

		else if(linkid == 'afterhours') {			
    	       var data = {
    	           text1: '<p>After Hours<br/><br/><br/><br/></p><p><span>Please call your Care Team to discuss <br/>your after hour snack options</span></p>',    	         
    	       };
    	       var page = new Information({className:'afterhours', data: data});
    	       page.render();
        }

		else if(alreadyselected == 'Y') {
    	      var data = {
    	           text1: '<p>This meal has already been ordered<br/><br/></p><p><span>Please call Ext. 24-3463 to <br/>discuss with your Food and Nutrition Team</span></p>',    	         
    	       };
    	       var page = new Information({className:'afterhours', data: data});
    	       page.render();       
            return true;
		}
		
		else {


			this.$('#mask').show();
			
			var context = this;
			$.doTimeout('dinemeals clock', 500, function() {
				context._selectMeal($jqobj);
			});
        
			return true;
		}
    },
    
    renderData: function() {
	this.$('#mask').show();
        
	var context = this;
    var data = this.data;
	this.$('#mask').hide();
	// show the div
	$('#' + this.wrapper + " #dinemeals").show();
	
		// start clock
        $.doTimeout('dinemeals clock', 60000, function() {
            var d = new Date();
            var format = 'dddd, mm.dd.yyyy - h:MM TT';
            var locale = window.settings.language;
            context.$('#banner #datetime').text(d.format(format, false, locale));
            return true;
        });
        $.doTimeout('dinemeals clock', true); // do it now
 
		
	$('#' + this.wrapper + " #dinemeals").addClass(this.classStr);
	
	var selections = this.$('#dinemeals #selections');
	var meals = Array();
	var mealsstring = '';
	var hintstring = '';
	var excludesnacks = false;

	$(data).find("Person").each(function() { 
		$(data).find("Meals").each(function() { 
			$(data).find("Meal").each(function() { 
				var meal = $(this).attr("Name");
				if(meal == 'PM SNACK') 
					meal = 'AFTERNOON SNACK'
				if(meal == 'AM SNACK') 
					meal = 'MORNING SNACK'
					
				var id = $(this).attr("Id");
				var dietorder = $(this).attr("DietOrderId");
				var diet = $(this).attr("Diet");
				
				var canorder = $(this).attr("CanOrder");
				var latestordertime = $(this).attr("LatestOrderTime");
				var alreadyselected = $(this).attr("AlreadySelected");
				var availableinfo = "Available for order"
				if(alreadyselected == 'Y') 
					availableinfo = 'Ordered <span class="small">Call Ext. 24-3463 to change order</span>'

					
				
				lastordertime = parseXMLTime(latestordertime);				
			
				//var lastordertime = formatdateTime("yyyy-mm-dd HH:MM:ss",lastordertime);
				//var now = formatdateTime("yyyy-mm-dd HH:mm:ss");
				var now = Date();
				datecompare = compareDates(lastordertime,now);				
				msg(datecompare + ' ' + lastordertime + ' ' + now);
				
				if(diet == 'RENAL DIET' || diet == 'LOW PHOSPHORUS' || diet == 'POTASSIUM, 2 GM' || diet == 'SODUIM DIET, 2 GM L/S' || diet == 'CARDIAC DIET' || diet == 'PROTEIN DIET, 40 GM' || diet == 'PROTEIN DIET, 70 GM' || diet == 'PROTEIN DIET, 100 GM' || diet == 'LOW FAT' ||diet == 'MINIMAL FAT')
					excludesnacks = true;
				
				if(meal=='BREAKFAST') {
					if (typeof meals[meal] == 'undefined' ) {
						if(canorder=='Y' && datecompare >= 0) {
						mealsstring = mealsstring + '<a href="#" class = "menu ' + meal + '" id="' + id + '" dietorder="' + dietorder + '" alreadyselected="' + alreadyselected + '">' + meal + ' TODAY</a>';						
						hintstring = hintstring + '<div id = "hint-' + id + '">' + availableinfo + '</div>';
						}
					} else {
						if(canorder=='Y' &&  datecompare >= 0) {							
							mealsstring = mealsstring + '<a href="#" class = "menu ' + meal + '" id="' + id + '" dietorder="' + dietorder + '" alreadyselected="' + alreadyselected + '">' + meal + ' TOMORROW</a>' ;
							hintstring = hintstring + '<div id = "hint-' + id + '">' + availableinfo + '</div>';
						}					
					}
				} 
				else if (typeof meals[meal] == 'undefined') {
				if(meal.indexOf('SNACK')>=1 && excludesnacks==true) {
				} else {
										
					if(canorder=='Y' &&  datecompare >= 0) {
						mealsstring = mealsstring + '<a href="#" class = "menu ' + meal + '" id="' + id + '" dietorder="' + dietorder + '" alreadyselected="' + alreadyselected + '">' + meal + '</a>';
						hintstring = hintstring + '<div id = "hint-' + id + '">' + availableinfo + '</div>';
					}
					}
				}
				meals[meal] = meal;
				
			});
		});
	});
			mealsstring = mealsstring + '<a href="#" class = "menu afterhours" id="afterhours" alreadyselected="Y">AFTER HOURS</a>';
			hintstring = hintstring + '<div id = "hint-afterhours">Please ask your Care Team</div>';
			this.$('#selections').html(mealsstring);
			this.$('#hints').html(hintstring);

    },
    
    /**********************************************************************************
     * Own public functions; Outside can call these functions
     *********************************************************************************/
	// uninit will be automatically called before page gets destroyed
	uninit: function() {
        $.doTimeout('dinemeals clock'); // stop clock
	},
 
	 
    /**********************************************************************************
     * Private functions; Starts with '_' only used internally in this class
     *********************************************************************************/
	_selectMeal: function($jqobj) {
		var dataobj = {};

		var currLabel = this.data.label
		var linkid = $jqobj.attr('id');
		var dietorder = $jqobj.attr('dietorder');
		dataobj = {};
		dataobj['mrn'] = window.settings.mrn;
		dataobj['vendor'] = window.settings.vendor;
		dataobj['meal'] = linkid;
		var url = "http://10.54.10.104:8936/GetFood"						

		var xml  = getXdXML(url, dataobj);      

		var FoodsResult = '';
		$(xml).find("GetFoodsResponse").each(function() {
			FoodsResult = $(xml).find("GetFoodsResult").text();			
		});
		
		var xmlObj = new DOMParser();   
		xmlObj = xmlObj.parseFromString(FoodsResult, "text/xml");     
		
		var deliverytimes = '';
		var subdata = '';
		$(xmlObj).find("DeliveryTimes").each(function() { 
			subdata = $(this);
		});
		
		window.settings.dietorder = dietorder;
		window.settings.meal = linkid;
		window.settings.deliverytimes = subdata;
		
			this.$('#mask').hide();
		   
		
		var page = new dinecourses({viewId: 'dinecourses', breadcrumb:currLabel, data: xmlObj});
		page.render();
		return true;

	}
});


