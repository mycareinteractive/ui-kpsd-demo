var AllPrograms = View.extend({

    id: 'allprograms',
    
    template: 'allprograms.html',
    
    css: 'allprograms.css',
    
    pageSize: 6,
    
    subPageSize: 6,
    
    /**********************************************************************************
     * Overwrite functions; from base class
     *********************************************************************************/

    navigate: function(key)	{
    	return false;
    },
    
    // When "ENTER" is pressed on a link, or when link is clicked by mouse or touch screen
    click: function($jqobj) {
    	var linkid = $jqobj.attr('id');
        var parentid = $jqobj.parent().attr('id');
        var type = $jqobj.attr('data-type');
        var itemData = this._getMenuItemData($jqobj);
        	
    	if($jqobj.hasClass('back')) { // back button
    		this.destroy();
    		return true;
    	} 
    	
    	// folder page up/down
    	if( linkid == 'next' || linkid == 'prev') {
    	    if (linkid=='next')
    	       this.scrollNext('#selections', 'vertical');
    	    else
    	       this.scrollPrev('#selections', 'vertical');
    	    this.$('#selections a.selected').removeClass('selected');
    	    this.$('#selection').hide();
    	    this.pagination('#selections', '#prev', '#next', 'vertical');
    	    return true;
    	}
    	
    	// asset page up/down
        if( linkid == 'subnext' || linkid == 'subprev') {
            if (linkid=='subnext')
               this.scrollNext('#subselections', 'vertical');
            else
               this.scrollPrev('#subselections', 'vertical');
            this.pagination('#subselections', '#subprev', '#subnext', 'vertical');
            return true;
        }
    	
    	// selecting a folder
        if($jqobj.hasClass('folder') && itemData) {
            this.$('#selections a.selected').removeClass('selected');
            $jqobj.addClass('selected');
            this._buildAssets(itemData.tagAttribute.hierarchyUID);
            this.pagination('#subselections', '#subprev', '#subnext', 'vertical');
            return true;
        }
        
    	// selecting a video
    	if($jqobj.hasClass('asset') && itemData) {
    	    var breadcrumb = this.$('#label p').text();
    	    var page = new ProgramDetail({className:'', parent:this, breadcrumb:breadcrumb, data: itemData});
    	    page.render();
    	    return true;
    	}
    	
    	return false;
    },
    
    focus: function($jqobj) {
        if($jqobj.hasClass('asset')) {
            this._super($jqobj);
            this.pagination('#subselections', '#subprev', '#subnext', 'vertical');
        }
        else if($jqobj.hasClass('folder')) {
            this._super($jqobj);
            this.pagination('#selections', '#prev', '#next', 'vertical');
        }
    },
    
    blur: function($jqobj) {
    	this._super($jqobj);
    },
    
    renderData: function() {
        $('<p></p>').html(this.data.label).appendTo(this.$('#label'));
        $('<p></p>').html(this.breadcrumb).appendTo(this.$('#breadcrumb'));
        this.label = this.data.label;
        this._buildCategories();
    },
    
    shown: function() {
        this.blur(this.$('#selections .folder:first'));
        this.focus(this.$('#selections .folder:first'));
        this.click(this.$('#selections .folder:first'));
        this.$('#selection').show();
    },
    
    refresh: function() {
        var $folder = this.$('#selections a.selected');
        var itemData = this._getMenuItemData($folder);
        if(itemData && itemData.tagAttribute)
            this._buildAssets(itemData.tagAttribute.hierarchyUID);
    },
    
    /**********************************************************************************
     * Own public functions; Outside can call these functions
     *********************************************************************************/
    
        /**********************************************************************************
     * Private functions; Starts with '_' only used internally in this class
     *********************************************************************************/
    _buildCategories: function () {
        var context = this;
        context.$('#selections').empty();
        
        var ewf = ewfObject();
        var folders = this.folders = getListEntry(ewf.allprograms).DataArea;
        
        if(folders && $.isArray(folders.ListOfEntry)) {
            $.each(folders.ListOfEntry, function(i, entry){
                if(entry.tagName == 'Folder') {
                    $('<a href="#" class="folder ellipsis"></a>').attr('data-index', i).attr('title', entry.tagAttribute.entryName)
                        .text(entry.tagAttribute.entryName).appendTo(context.$('#selections'));
                }
            });
            
            // pad some extra lines to fill the last page
            var padNum = this.pageSize - (folders.ListOfEntry.length % this.pageSize);
            if(padNum == 6) padNum = 0;
            for(var i=0; i<padNum; i++) {
                $('<div class="padding"></div>').appendTo(context.$('#selections'));
            }
        }
        
        // hide page up/down if less than one page
        if(folders.ListOfEntry.length <= this.pageSize) {
            this.$('#buttons #prev').hide();
            this.$('#buttons #next').hide();
        }
        else {
            this.$('#buttons #prev').show();
            this.$('#buttons #next').show();
        }
    },
    
    _buildAssets: function (huid) {
        var context = this;
        
        context.$('#selection #subselections').empty();
        
        var assets = this.assets = getListEntry(huid).DataArea;
        
        if(assets && $.isArray(assets.ListOfEntry)) {
            $.each(assets.ListOfEntry, function(i, entry){
                if(entry.tagName == 'Asset') {
                    var isBookmarked = (entry.tagAttribute.ticketIDList && entry.tagAttribute.ticketIDList != '');
                    var $entry = $('<a href="#" class="asset ellipsis"></a>').attr('data-index', i).attr('title', entry.ListOfMetaData.Title).text(entry.ListOfMetaData.Title)
                    if(isBookmarked)
                        $entry.addClass('selected');
                    $entry.appendTo(context.$('#selection #subselections'));
                }
            });
            
            // pad some extra lines to fill the last page
            var padNum = this.pageSize - (assets.ListOfEntry.length % this.pageSize);
            if(padNum == 6) padNum = 0;
            for(var i=0; i<padNum; i++) {
                $('<div class="padding"></div>').appendTo(context.$('#selection #subselections'));
            }
        }
        
        // hide page up/down if less than one page
        if(assets.ListOfEntry.length <= this.subPageSize) {
            this.$('#subbuttons #subprev').hide();
            this.$('#subbuttons #subnext').hide();
        }
        else {
            this.$('#subbuttons #subprev').show();
            this.$('#subbuttons #subnext').show();
        }
        
        this.$('#selection').show();
        this.$('#selection #subselections').scrollTop(0);
    },
    
    _getMenuItemData: function ($obj) {
        var itemData = null;
        var itemIndex = $obj.attr('data-index');
        var entries = null;
        
        if($obj.hasClass('folder'))
            entries = this.folders;
        else if($obj.hasClass('asset'))
            entries = this.assets;
            
        if(itemIndex && entries && entries.ListOfEntry) {
            itemData = entries.ListOfEntry[itemIndex];
        }
        
        return itemData;
    }
});    