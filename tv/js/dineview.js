var dineview = View.extend({

    id: 'dineview',
    
    template: 'dineview.html',
    
    css: 'dineview.css',
    
    pageSize: 8,
	
	course: null,
    
    /**********************************************************************************
     * Overwrite functions; from base class
     *********************************************************************************/

    navigate: function(key)	{
    	if(key == 'UP' || key == 'DOWN') {
    		this.changeFocus(key, '#selections');	
    		return true;
    	}
    	return false;
    },
    
    // When "ENTER" is pressed on a link, or when link is clicked by mouse or touch screen
    click: function($jqobj) {
   var currLabel = this.data.label;
	var linkid = $jqobj.attr('id');
        var parentid = $jqobj.parent().attr('id');
        var type = $jqobj.attr('data-type');
		var currlink = this.currsel;
		var keyname = $jqobj.attr('keyname');
        this.coursemax = window.settings.coursemax;
        var coursemax = this.coursemax;

        msg(linkid);
		//var itemData = this._getMenuItemData($jqobj);
		if(linkid == 'down') { // back button
    		msg('down');
    		return true;
    	} 
    	 				 
        if(linkid == 'cancel' ) {

				var data = {
    	           text1: '<p>Warning</p><br/><br><br><br><p>Any orders that<br/>have not been placed<br/>will be canceled.<br/><br/>Would you like to cancel this order?</p>',
    	       };
			
    	       var page = new warning({className:'dinedecline', data: data});
    	       page.render();
			   return true;

		}		  

		if($jqobj.hasClass('back')) { // back button
            window.pages.closePage('dineitems');
            window.pages.closePage('dinesandwich');
            window.pages.closePage('dinecondiments');

    		this.destroy();
    		return true;
    	} 
    	
		if(linkid == 'exit') {			
		    keypressed('CLOSEALL');
            keypressed(216);    //force going back to main menu

			this.destroy();
            return false;
        }

    	// page up/down
    	if( linkid == 'next' || linkid == 'prev') {
    	    var pos = this.$('#selections').scrollTop();
    	    var height = this.$('#selections').height();
    	    pos = (linkid=='next')? pos+height: pos-height;
    	    this.$('#selections').scrollTop(pos);
    	    return true;
    	}
    	
		if(linkid == 'submit') { // place item			
        	if(window.meals.length>=1) {				
                // Or a speical link in the content that triggers next page
                var subdata = window.settings.deliverytimes;
                var page = new dinedeliverytime({viewId: 'dinedeliverytime', breadcrumb:currLabel, data: subdata});
                page.render();
                return true;	
            } else {
                   var data = {
    	           text1: '<p>No Items Selected<br/><br/></p><p><span>Please make your selections and then view your order', 
    	       };
    	       var page = new Information({className:'dinedecline', data: data});
    	       page.render();
            }
					
    	} 
    	
		if(linkid == 'help') {
    	       var data = {
    	           text1: '<p>Hungry?</p><p><span>Please press the red call <br/>button and a member of your <br/>Care Team will be by shortly.</span></p>',    	         
    	       };
    	       var page = new Information({className:'dinedecline', data: data});
    	       page.render();				
				return true;
		}

		if($jqobj.hasClass('item')) {
            this.currsel = this.$('#selections a.active');
            currsel = this.currsel;
            var currqty =currsel.attr("qty");
                        
            this.$('#qty').attr('style','display:block');		
            this.$('#qty #itemqty').text(currqty);
            
		}
        
        
		if($jqobj.hasClass('up')) {           
            var currsel = this.currsel;
            var currqty =currsel.attr("qty");
            var currid = currsel.attr("data-index");
            var carbs = currsel.attr("carbs");
			var maxcarbs = window.settings.maxcarbs;
            var max = currsel.attr("max");
			var totalcarbs = window.settings.totalcarbs;
            var newqty = Number(currqty) + 1;
            if(newqty>=9) 
                newqty = 9;
                
             var checkcarbs = Number(carbs*newqty) + Number(totalcarbs);
             msg('check carbs' + checkcarbs + ' carbs '+ carbs + ' max carbs ' + maxcarbs); 
             if(checkcarbs > maxcarbs) {
					var data = {
						text1: '<p>Warning<br/><br/><br/><span>You are over your carbohydrate limit.<br/><br/>Please select checked item and click "Remove Item" <br/>prior to selecting another</p>'
					};
					var page = new Information({className:'maxselected', data: data});
					page.render();	
					return true;
				}
                
                if(newqty>= max && max!='') {
					var data = {
						text1: '<p>Warning<br/><br/><br/><span>You are over your limit <br/>for this course.<br/><br/>Please select checked item and click "Remove Item" <br/>prior to selecting another</p>'
					};
					var page = new Information({className:'maxselected', data: data});
					page.render();	
					return true;
				}


            this.$('#qty').attr('style','display:block');		
            this.$('#qty #itemqty').text(newqty);
            this.$('#qty #itemcarbs').text(checkcarbs);
            
            
            	var items = window.meals;  
				var i = 0;		
				for(var i=0; i<items.length; i++) {
					var item = items[i];
					var id = item["id"];
					msg('id ' + id + ' currid ' + currid);
					if(id==currid)
						window.meals[i].qty = newqty;                        
				}		
                //this._buildItems();
                currsel.addClass('active');
                this.$('#itemqty').text(newqty);
                currsel.attr('qty',newqty);
                this.$('#selections a.active p.qty').text(newqty)
                this.$('#qty').attr('style','display:block');		
                this.$('#selections a.active p.itemcarbs').text(carbs*newqty);
            
		}

        
        if($jqobj.hasClass('down')) {           
            var currsel = this.currsel;
            var currqty =currsel.attr("qty");
            var currid = currsel.attr("data-index");
            
            var newqty = Number(currqty) - 1;
            if(newqty<=1)
                newqty = 1;
            this.$('#qty').attr('style','display:block');		
            this.$('#qty #itemqty').text(newqty);
            
            	var items = window.meals;  
				var i = 0;		
				for(var i=0; i<items.length; i++) {
					var item = items[i];
					var id = item["id"];
					msg('id ' + id + ' currid ' + currid);
					if(id==currid)
						window.meals[i].qty = newqty;
				}		
                //this._buildItems();
                currsel.addClass('active');
                this.$('#itemqty').text(newqty);
                currsel.attr('qty',newqty);
                this.$('#selections a.active p.qty').text(newqty)
                this.$('#qty').attr('style','display:block');		
                this.$('#qty #itemqty').text(newqty);
            
		}

        
        
        if($jqobj.hasClass('close')) {
            this.$('#qty').attr('style','display:none');		
		}
    	
        if($jqobj.hasClass('remove')) {
            window.pages.closePage('dineitems');
            window.pages.closePage('dinesandwich');
            window.pages.closePage('dinecondiments');
            var currsel = this.currsel;
            var currid = currsel.attr("data-index");
            var page = window.pages.findPage('dineitems');
            if(!page)
                var page = window.pages.findPage('dinesandwichboard');
            if(!page)
                var page = window.pages.findPage('dinecondiments');
                
				var items = window.meals;  
				var i = 0;		
				for(var i=0; i<items.length; i++) {
					var item = items[i];
					var id = item["id"];
					msg('id ' + id + ' currid ' + currid);
					if(id==currid)
						window.meals.splice(i,1);
				}		
                this._buildItems();
                return true;

        }
        
    	return false;
    },
    
    focus: function($jqobj) {
		
        if($jqobj.parent().attr('id') == 'selections' || $jqobj.attr('id')=='order') {
            this._super($jqobj);
    	}
    },
    
    blur: function($jqobj) {
    	this._super($jqobj);
    },
    
    renderData: function() {
        this._buildItems();
    },
    
    refresh: function() {
        this._buildItems();
    },
    
    /**********************************************************************************
     * Own public functions; Outside can call these functions
     *********************************************************************************/
    
        /**********************************************************************************
     * Private functions; Starts with '_' only used internally in this class
     *********************************************************************************/
    
		
	_buildItems: function () {
        this.$('#qty').attr('style','display:none');		
        var context = this;
        context.$('#selections').empty();
        var maxcarbs = window.settings.maxcarbs;
        this.$('#nutrients #max').html(maxcarbs);			
        var totalcarbs = window.settings.totalcarbs;
        this.$('#nutrients #totalcarbs').html(totalcarbs);			

		if(maxcarbs>=1) {			
			this.$('#nutrients').attr('style','display:block');			
			this.$('#nutrients #max').html(maxcarbs);			
		} else {
			this.$('#nutrients').attr('style','display:none');		
            this.$('#nutrient').attr('style','display:none');		
        }

        
		var items = window.meals;  
		var i = 0;
		
		for(i=0; i<items.length; i++) {
				var item = items[i];		
				var id = item["id"];
				var title = item["title"];
				var keyname = item["keyname"];
				var qty = item["qty"];
				var carbs = item["carbs"];
				var max = item["max"];
				var itemselected = window.meals;
				msg(itemselected["id"+id]);
				var $entry = $('<a href="#" class="item ellipsis"></a>').attr('data-index', id).attr('carbs',carbs).attr('qty',qty).attr('max',max).attr('title', title).text(title);
				
				$('<p class="qty" span"></span></p>').text(qty).prependTo($entry);
                if(maxcarbs>=1) 
					$('<span class="itemcarbs" style="float:right"></span>').text(carbs).appendTo($entry);

                				
				$entry.appendTo(context.$('#selections'));
				cnt = cnt + 1;
		
		}		
       	    // pad some extra lines to fill the last page
            var cnt = i;
            //var padNum = this.pageSize - (cnt % this.pageSize);
            //if(padNum == 8) padNum = 0;
            //for(var i=0; i<padNum; i++) {
             //   $('<div class="padding"></div>').appendTo(context.$('#selections'));
            //}

            if(cnt <= this.pageSize) {
                this.$('#buttons #prev').hide();
                this.$('#buttons #next').hide();
            } else {
                this.$('#buttons #prev').show();
                this.$('#buttons #next').show();
            }
             
                    
    },
	
});    