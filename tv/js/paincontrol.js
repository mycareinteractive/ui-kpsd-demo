var PainControl = View.extend({

    id: 'paincontrol',
    
    template: 'paincontrol.html',
    
    css: 'paincontrol.css',
	
    extensionIP: '10.7.251.10',    
    /**********************************************************************************
     * Overwrite functions; from base class
     *********************************************************************************/

    navigate: function(key)	{
    	if(key == 'LEFT' || key == 'RIGHT') {
    	    this.changeFocus(key, '#painbuttons', 'a');
    	    return true;
    	}
    	return false;
    },
    
    // When "ENTER" is pressed on a link, or when link is clicked by mouse or touch screen
    click: function($jqobj) {
    	var linkid = $jqobj.attr('id');
        
    	if(linkid == 'indicator') {
    		this.$('#external').addClass('popup');
    		return true;
    	}
    	else if(linkid == 'hide') {
    		this.$('#external').removeClass('popup');
    		return true;
    	}
    	else if(linkid == 'connect') {
    		this._loadExtension();
    		return true;
    	}
    	
        if($jqobj.hasClass('back')) { // back button
			if(this.closeall==true) {			
				keypressed('CLOSEALL');
				keypressed(216);    //force going back to main menu
			}
				
    		this.destroy();
    		return true;
    	} 
        else if($jqobj.hasClass('painbutton')) {
        	var scale = $jqobj.attr('data-scale');
        	msg('pain scale rated ' + scale);
        	if(scale >= 6)
        		this._triggerExtension();
        }
    	
    	return true;
    },
    
    shown: function() {
    	this.$('#external').removeClass('popup');
    	this.changeFocus('RIGHT', '#painbuttons', 'a');
    },
    
    renderData: function() {
        var context = this;
        var data = this.data;
    	this.$('#extension').load(function() {
    		context._onFrameOpen();
    	});
        this._loadExtension();
    },
    
    uninit: function() {
        $.doTimeout('extension iframe timer');
    },
    
    /**********************************************************************************
     * Own public functions; Outside can call these functions
     *********************************************************************************/
    
    /**********************************************************************************
     * Private functions; Starts with '_' only used internally in this class
     *********************************************************************************/
    _triggerExtension: function() {
    	var context = this;
    	context._openFrame('http://' + context.extensionIP + '/ei/workflow_engine/create/KaiserSimulator/demo?Kaiser.alert_type=NurseCall');
    	context.$('#extension').one('load', function() {
    		context.$('#painbuttons').hide();
    		context.$('#indicator').hide();
    		context.$('#instruction').hide();
    		context.$('#feedback').show();
    	});
    },
    
    _checkExtensionOnline: function() {
    	var context = this;
    	try {
    	var iframe = context.$('#extension').contents();
			if($('a[href="/ei/accounts/logout"]', iframe).length==0) {
				this.$('#indicator').removeClass('online');
				context._loginExtension();
				return false;
			}
			else {
				this.$('#indicator').addClass('online');
				context._loadExtension();
				return true;
			}
    	}
    	catch(e){}
    },
    
    _loadExtension: function() {
    	var context = this;
    	context._openFrame('http://' + context.extensionIP + '/ei/w/545');
    },
    
    _loginExtension: function() {
    	var context = this;
    	
    	var loading = this._openFrame('http://' + context.extensionIP + '/ei/accounts/login');
    	
    	if(loading) {
	    	context.$('#extension').one('load', function() {
	    		context._autoLogin();
	    	});
	    	context.$('#extension').one('load', function() {
				context._loadExtension();
			});
    	}
    	else {
    		context._autoLogin();
    	}
    },
    
    _autoLogin: function() {
    	var context = this;
    	try {
    		context.$('#external').addClass('popup');
    		var iframe = context.$('#extension').contents();
    		$('input#user_login', iframe).val('demo');
			$('input#user_password', iframe).val('demo');
			$('button#submit_button', iframe).click();
		}
		catch(e){}
    },
    
    _openFrame: function(url) {
    	var context = this;
    	try {
	    	if(context.$('#extension').contents()[0].location.href.indexOf(url) == 0) { // find real URL in iframe
	    		return false;
	    	}
    	}
    	catch(e) {
    		if(context.$('#extension').attr('src') == url) {
        		return false;
        	}
    	}
    	$.doTimeout('extension iframe timer');
    	context.$('#extension').attr('src', url);
    	return true;
    },
    
    _onFrameOpen: function() {
    	var context = this;
    	$.doTimeout('extension iframe timer');
    	$.doTimeout('extension iframe timer', 30000, function() {
            context._checkExtensionOnline();
            return true;
        });       
        $.doTimeout('extension iframe timer', true);
    }
});    