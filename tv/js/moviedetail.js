var MovieDetail = View.extend({

    id: 'moviedetail',
    
    template: 'moviedetail.html',
    
    css: 'moviedetail.css',
    
    /**********************************************************************************
     * Overwrite functions; from base class
     *********************************************************************************/

    navigate: function(key)	{
    	if(key == 'UP' || key == 'DOWN') {
    		this.changeFocus(key, '#buttons');	
    		return true;
    	}
    	return false;
    },
    
    // When "ENTER" is pressed on a link, or when link is clicked by mouse or touch screen
    click: function($jqobj) {
    	var linkid = $jqobj.attr('id');
        var parentid = $jqobj.parent().attr('id');
        var type = $jqobj.attr('data-type');
        	
    	if($jqobj.hasClass('back')) { // back button
    		this.destroy();
    		return true;
    	}
    	
    	if(linkid == 'play') {   // remove bookmark
    	    var breadcrumb = this.$('#label p').text();
            var page = new VideoPlayer({className:'', parent:this, breadcrumb:breadcrumb, data: this.data, bookmark: true, type: 'movie'});
            page.render();
            return true;
    	}

    },
    
    focus: function($jqobj) {
        if($jqobj.parent().attr('id') == 'buttons') {
            this._super($jqobj);
    	}
    },
    
    blur: function($jqobj) {
    	this._super($jqobj);
    },
    
    renderData: function() {
        var context = this;
        var isBookmarked = false;
        
        // fill in title and duration
        $('<p></p>').html(this.breadcrumb).appendTo(this.$('#breadcrumb'));
        $('<p></p>').html(this.label).appendTo(this.$('#label'));
        if (this.data.tagName == 'Asset') { // an asset from AllPrograms
            var sub = this.data;
            if(sub.tagAttribute && sub.tagAttribute.ticketIDList && sub.tagAttribute.ticketIDList != '') {
                isBookmarked = true;
            }
            if(sub.ListOfMetaData) {
                var title = sub.ListOfMetaData.Title;
                var year = sub.ListOfMetaData.YearOfRelease;
                var duration = context._calcDuration(sub.tagAttribute.duration);
                var rating = context._calcRating(sub.ListOfMetaData.Rating);
                var description = sub.ListOfMetaData.LongDescription;
                this.$('#selection #title').text(title);
                this.$('#selection #year').text(year);
                this.$('#selection #length').text(duration);
                this.$('#selection #rating').text(rating);
                this.$('#selection #description').text(description);
                
                var poster = sub.ListOfMetaData.PosterBoard;
                poster = poster.split(',')[0];
                $('<img></img>').attr('src', poster).appendTo(this.$('#poster'));
            }
        }
        
        // determine which buttons to show up
        if(isBookmarked) {
            this.$('#buttons a#resume').show();
            this.$('#buttons a#play').show();
        }
        else {
            this.$('#buttons a#resume').hide();
            this.$('#buttons a#play').show();
        }
    },
    
    /**********************************************************************************
     * Own public functions; Outside can call these functions
     *********************************************************************************/
    
    /**********************************************************************************
     * Private functions; Starts with '_' only used internally in this class
     *********************************************************************************/
    _calcDuration: function (duration) {
        if(!duration)
            return '00:00';
        var str = '';
        if(duration>=3600) {
            var h = Math.floor(duration/3600);
            str += h + 'hr ';
            duration = duration%3600;
        }
        if(duration>=60) {
            var mm = Math.floor(duration/60);
            str += (mm<10?'0':'') + mm + 'm';
            duration = duration%60;
        }
        //var ss = duration;
        //str += (ss<10?'0':'') + ss;
        return str;
    },
    
    _calcRating: function (code) {
        var rating      = 'NR';
        var ratingA     = ['NR', 'G', 'PG', 'PG-13', 'R', 'NC-17', 'Adult', 'NR', 'NR', 'NR'];        
        rating = ratingA[code-1];
        return rating;
    }
    
});    