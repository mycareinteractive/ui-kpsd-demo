(function ($) {
    $(document).ready(function () {
        $('.swipebox').swipebox({
            hideBarsOnMobile: false,
            afterSwipeUp: function (url) {
                console.log('cast ' + url);
                var payload = $('<div></div>').append($('<img>').attr('src', url)).html();
                cast(payload);
            }
        });

        // PubNub real-time API
        window.pubnub = PUBNUB.init({
            publish_key: 'pub-c-e270c713-246e-443f-918e-d12d2fdc5c6d',
            subscribe_key: 'sub-c-567191ae-19f8-11e4-a9f2-02ee2ddab7fe',
            uuid: 'mobile'
        });

        $('a#connect').tap(function (e) {
            if ($(this).attr('data-theme') == 'a') {
                showMessageBox('Connecting to Aceso UpCare...', 5000);
                connect();
            }
            else {
                disconnect();
            }
        });

        $('a#risk, a#alarm').tap(function (e) {
            var id = $(this).attr('id');
            fall(id);
        });
        $('a#person').tap(function (e) {
            var title = $(this).attr('data-title');
            var content = $(this).attr('data-content');
            var image = $(this).attr('data-image');
            image = $('<a/>').attr('href', image)[0].href; // convert to absolute path
            notify(title, content, image);
        });
    });

    var connect = function () {
        try {
            window.pubnub.uuid(function (uuid) {
                window.subchannel = 'multiscreen-' + uuid;
                console.log('subchannel = ' + window.subchannel);
                // waiting for subchannel presence
                window.pubnub.subscribe({
                    channel: window.subchannel,
                    message: function (m) {
                        console.log(JSON.stringify(m));
                        if (m.action === 'join') {
                            console.log(m.uuid + ' connected');
                            $('#connect').attr('data-theme', 'd').removeClass('ui-btn-up-a').addClass('ui-btn-up-d');
                            $('a.sim').hide();
                            hideMessageBox();
                        }
                        else if (m.action === 'leave') {
                            disconnect();
                        }
                    }
                });

                // send subchannel to TV
                window.pubnub.publish({
                    channel: 'kpsd_master',
                    message: {category: 'multiscreen', name: 'ZIPP, MD', subchannel: window.subchannel}
                });
            });
        }
        catch (e) {
        }
    };

    var disconnect = function () {
        try {
            if (window.pubnub && window.subchannel) {
                console.log('stop multiscreen');
                $('#connect').attr('data-theme', 'a').removeClass('ui-btn-up-b').addClass('ui-btn-up-a');
                $('a.sim').show();
                showMessageBox('Aceso UpCare Disconnected', 2000);

                // disconnect TV
                window.pubnub.publish({
                    channel: window.subchannel,
                    message: {action: 'stop'}
                });
                window.pubnub.unsubscribe({
                    channel: window.subchannel
                });
            }
        }
        catch (e) {
        }
    };

    var cast = function (payload) {
        showMessageBox('Beam me up...', 2000);
        window.pubnub.publish({
            channel: window.subchannel,
            message: payload
        });
    };

    var fall = function (id) {
        showMessageBox('Trigger Fall Risk', 2000);
        var cat = (id == 'alarm') ? 'fallalarm' : 'fallrisk';
        window.pubnub.publish({
            channel: 'kpsd_master',
            message: {category: cat}
        });
    };


    //var notify = function (msg) {
    //    showMessageBox('Sending Notification', 2000);
    //    window.pubnub.publish({
    //        channel: 'kpsd_master',
    //        message: {category: 'notification', message: msg}
    //    });
    //};

    var notify = function (title, content, image) {
        showMessageBox('Sending Notification', 2000);
        var msg = {};
        msg['title'] = title;
        msg['content'] = content;
        msg['image'] = image;
        window.pubnub.publish({
            channel: 'kpsd_master',
            message: {category: 'notification', message: msg}
        });
    };

    var showMessageBox = function (message, sleep) {
        $('#messagebox p strong').text(message);
        $('#messagebox').popup('open');
        $("#messagebox").popup("reposition", {positionTo: 'window'});
        $.doTimeout('close message');
        $.doTimeout('close message', sleep, function () {
            $('#messagebox').popup('close');
        });
    };

    var hideMessageBox = function () {
        $.doTimeout('close message');
        $('#messagebox').popup('close');
    };

})(jQuery);